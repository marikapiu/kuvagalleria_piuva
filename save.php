<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
    <title>Document</title>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Kuvagalleria</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="add.php">Lisää
                        <span class="sr-only">(current)</span>
                        <i class="fas fa-camera"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
<div class="container">
    <div class="row">
        <div class="col">
        </div>
    </div>
</div>
<?php
include 'lib/ImageResize.php';
use \Gumlet\ImageResize;
?>
<?php require_once 'inc/top.php'; ?>
<?php
if ($_FILES['file']['error'] === UPLOAD_ERR_OK) {
    if ($_FILES['file']['size'] > 0) {
        $type = $_FILES['file']['type'];
        if ($type === 'image/png' || $type === 'image/jpg' || $type === 'image/jpeg') {
            $file = basename($_FILES['file']['name']);
            $folder = 'uploads/';
            if (move_uploaded_file($_FILES['file']['tmp_name'],"$folder/$file")) {
                try {
                    $image = new ImageResize("$folder$file");
                    $image->resizeToWidth(150);
                    $image->save($folder . 'thumbs/' . $file);
                print "<p>Kuva on tallennettu palvelimelle!</p>";
            }
            catch (Exception $ex) {
                print "<p>Kuvan tallennuksessa tapahtui virhe!" . $ex->get_message() . "</p>";
            }
            } 
            else {
                print "<p>Kuvan tallennuksessa tapahtui virhe</p>";
            }
        }
    }
    else {
        print "<p>Voit ladata vain png- ja jpg-kuvia!</p>";
    } 
}
else {
    print "<p>Virhe kuvan lataamisessa! Virhekoodi: " . $_FILES['file']['error'] . "</p>";
}
?>
<a href='index.php'>Selaa kuvia</a>
<?php require_once 'inc/bottom.php'; ?>
</body>
<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/812c926da6.js" crossorigin="anonymous"></script>
<script src="js/image.js"></script>
</html>