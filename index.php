<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Kuvagalleria</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
    
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Kuvagalleria</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="add.php">Lisää
                        <span class="sr-only">(current)</span>
                        <i class="fas fa-camera"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
<div class="container">
    <div class="row">
        <div class="col">
        </div>
    </div>
</div>


<form action="save.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="file">Tiedosto</label>
        <input type="file" class="form-control" id="file" name="file">
        <button class="btn btn-primary">Lataa</button>
</div>
</form>

<?php require_once 'inc/top.php'; ?>
<?php
$folder = 'uploads';
$handle = opendir($folder);
if ($handle) {
    print "<div id='images'></div>";
    print "<div class='card-group'>";
    print "<div class='row'>";
    while (false !== ($file = readdir($handle))) {
            $file_ending = explode('.',$file);
            $ext = end($file_ending);
            if (strtoupper($ext) === 'PNG' || strtoupper($ext) === 'JPG' || strtoupper($ext) ==='JPEG') {
            $path = $folder . '/' . $file;
            $thumbs_path = $folder . '/thumbs/' . $file;
            ?>
            <div class="card p-2 col-lg-3 col-md-4 col-sm-6">
                <a data-fancybox="gallery" href="<?php print $path; ?>">
                <img class="card-img-top" src="<?php print $thumbs_path; ?>">
            </a>
            <div class="card-body">
                <p class="card-text"><?php print $file; ?></p>
        </div>
    </div>
    <?php
        }
    }
}
?>
<?php require_once 'inc/bottom.php'; ?>
<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/812c926da6.js" crossorigin="anonymous"></script>
<script src="js/image.js"></script>
</body>
</html>